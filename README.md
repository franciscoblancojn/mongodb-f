# mongodb-f

This is a simple way to connect to mongodb

# Use

## Connect

```javascript
import { mongodbF } from "mongodb-f";

const db = await mongodbF({
    url: "url of database mongo",
    database: "name database",
});
```

## GET

```javascript
const result = await db.get({
    table: "name table o collection",
    query: {
        // query of get (optional)
    },
    options: {
        //options for query mongodb (optional)
    },
    preToArray: (e) => e, // function for parse array pre return (optional)
});
```

## POST

```javascript
const result = await db.post({
    table: "name table o collection",
    data: {
        //data to save 
        // if send object create one
        // id send array create multiple
    },
    options: {
        //options for create mongodb (optional)
    },
});
```

## PUT

```javascript
const result = await db.put({
    table: "name table o collection",
    query: {
        // query of update
    },
    data: {
        $set: {
            // data for update
        },
    },
    multiple: true, // if update multiple (default true) (optional)
    options: {
        //options for update mongodb (optional)
    },
});
```

## DELETE

```javascript
const result = await db.delete({
    table: "name table o collection",
    query: {
        // query of delete
    },
    multiple: true, // if delete multiple (default true) (optional)
    options: {
        //options for delete mongodb (optional)
    },
});
```
