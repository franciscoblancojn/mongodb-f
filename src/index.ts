import { connect, connectProps } from "./connect";
import { get, getProps } from "./get";
import { post, postProps } from "./post";
import { put, putProps } from "./put";
import { _delete, deleteProps } from "./delete";

export type mongodbFProps = connectProps;

export const mongodbF = ({ ...props }: mongodbFProps) => {
    return {
        get: (getProps: getProps) =>
            connect({
                ...props,
                action: get(getProps),
            }),

        post: (postProps: postProps) =>
            connect({
                ...props,
                action: post(postProps),
            }),

        put: (putProps: putProps) =>
            connect({
                ...props,
                action: put(putProps),
            }),

        delete: (deleteProps: deleteProps) =>
            connect({
                ...props,
                action: _delete(deleteProps),
            }),
    };
};
