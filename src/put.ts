import { Db, ObjectId } from "mongodb";

export interface putProps {
    table: string;
    data: any;
    query: any;
    options?: any;
    multiple?: boolean;
}

export const put =
    ({ table, data, query, options = {}, multiple = true }: putProps) =>
    async (dbMongo: Db) => {
        if (query._id) {
            query._id = new ObjectId(query._id);
        }
        const collection = dbMongo.collection(table);
        let response;
        if (multiple) {
            response = await collection.updateMany(query, data, options);
        } else {
            response = await collection.updateOne(query, data, options);
        }
        const result = await response;
        return result;
    };
