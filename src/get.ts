import { Db, ObjectId } from "mongodb";

export interface getProps {
    table: string;
    query?: any;
    options?: any;
    preToArray?: (e: any) => any;
}

export const get =
    ({ table, query = {}, options = {}, preToArray = (e) => e }: getProps) =>
    async (dbMongo: Db) => {
        if (query._id) {
            query._id = new ObjectId(query._id);
        }
        const collection = dbMongo.collection(table);
        const response = await preToArray(
            collection.find(query, options)
        ).toArray();
        const result = await response;
        return result;
    };
