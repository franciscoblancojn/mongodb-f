import { MongoClient, Db } from "mongodb";

export interface connectProps {
    url: string;
    database: string;
}
export interface connectAcctionProps extends connectProps {
    action: (db: Db) => Promise<any>;
}

export const connect = async ({
    url,
    database,
    action,
}: connectAcctionProps) => {
    const client = new MongoClient(url);
    try {
        if (!client) {
            throw new Error("Error conection database Mongo");
        }
        await client.connect();
        const dbMongo = client.db(database);
        const result = await action(dbMongo);
        return {
            type: "ok",
            result,
        };
    } catch (error) {
        return {
            type: "error",
            error: `${error}`,
        };
    } finally {
        await client.close();
    }
};
