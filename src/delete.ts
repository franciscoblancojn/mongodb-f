import { Db, ObjectId } from "mongodb";

export interface deleteProps {
    table: string;
    query: any;
    options?: any;
    multiple?: boolean;
}

export const _delete =
    ({ table, query, options = {}, multiple = true }: deleteProps) =>
    async (dbMongo: Db) => {
        if (query._id) {
            query._id = new ObjectId(query._id);
        }
        const collection = dbMongo.collection(table);
        let response;
        if (multiple) {
            response = await collection.deleteMany(query, options);
        } else {
            response = await collection.deleteOne(query, options);
        }
        const result = await response;
        return result;
    };
