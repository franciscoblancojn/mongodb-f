import { Db } from "mongodb";

export interface postProps {
    table: string;
    data: any;
    options?: any;
}

export const post =
    ({ data, table, options = {} }: postProps) =>
    async (dbMongo: Db) => {
        const collection = dbMongo.collection(table);
        let response;
        if (Array.isArray(data)) {
            response = await collection.insertMany(data, options);
        } else {
            response = await collection.insertOne(data, options);
        }

        const result = await response;
        return result;
    };
